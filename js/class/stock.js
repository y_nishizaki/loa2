/**
 * stock.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Stock = new function() {

	var self = function Stock(own_units) {
		UnitManager.apply(this, arguments);
	}

	var uber = UnitManager.prototype;

	self.prototype = extend(uber, {
		constructor: self
	});

	return self;
}



