/**
 * spriteNumber.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SpriteNumber = Class.create(Sprite,{
	initialize: function(num, critical) {
		var width = 30;
		var height = 30;
		if (typeof num === 'undefined')			num = 0;
		if (typeof critical === 'undefined')	critical = false;

		// Parent Call
		Sprite.call(this, width, height);

		// Number Settings
		this.setImage(critical);
		this.setNumber(num);
	}

	, setImage: function(critical) {
		// Normal
		this.image = Game.Seq.core.assets['images/loa/number01.png'];
		// Critical
		if (critical) {
			this.image = Game.Seq.core.assets['images/loa/number02.png'];
		}
	}

	, setNumber: function(num) {
		num = parseInt(num);
		switch (num) {
			case 1:
				this.frame = 0;
				break;
			case 2:
				this.frame = 1;
				break;
			case 3:
				this.frame = 2;
				break;
			case 4:
				this.frame = 3;
				break;
			case 5:
				this.frame = 4;
				break;
			case 6:
				this.frame = 5;
				break;
			case 7:
				this.frame = 6;
				break;
			case 8:
				this.frame = 7;
				break;
			case 9:
				this.frame = 8;
				break;
			case 0:
				this.frame = 9;
				break;
			default:
				break;
		}
	}



});


