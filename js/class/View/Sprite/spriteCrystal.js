/**
 * spriteCrystal.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SpriteCrystal = Class.create(Group,{
	initialize: function(level) {
		Group.call(this);

		if (typeof level === 'undefined')	return false;
		// Level Settings
		this.setLevel(level);

	}

	, setLevel: function(level) {
		switch (level) {
			case 1:
				this.crystal1_1 = new Sprite(21, 25);
				this.crystal1_1.image = Game.Seq.core.assets['images/loa/crystal.png'];
				this.crystal1_1.x = 5;
				this.crystal1_1.y = 0;
				this.addChild(this.crystal1_1);
				break;
			case 2:
				this.crystal2_1 = new Sprite(21, 25);
				this.crystal2_1.image = Game.Seq.core.assets['images/loa/crystal.png'];
				this.crystal2_1.x = 0;
				this.crystal2_1.y = 0;
				this.addChild(this.crystal2_1);
				this.crystal2_2 = new Sprite(21, 25);
				this.crystal2_2.image = Game.Seq.core.assets['images/loa/crystal.png'];
				this.crystal2_2.x = 11;
				this.crystal2_2.y = 0;
				this.addChild(this.crystal2_2);
				break;
			case 3:
				this.crystal3_1 = new Sprite(21, 25);
				this.crystal3_1.image = Game.Seq.core.assets['images/loa/crystal.png'];
				this.crystal3_1.x = 0;
				this.crystal3_1.y = 0;
				this.addChild(this.crystal3_1);
				this.crystal3_2 = new Sprite(21, 25);
				this.crystal3_2.image = Game.Seq.core.assets['images/loa/crystal.png'];
				this.crystal3_2.x = 11;
				this.crystal3_2.y = 0;
				this.addChild(this.crystal3_2);
				this.crystal3_3 = new Sprite(21, 25);
				this.crystal3_3.image = Game.Seq.core.assets['images/loa/crystal.png'];
				this.crystal3_3.x = 5;
				this.crystal3_3.y = -5;
				this.addChild(this.crystal3_3);
				break;
			default:
				break;
		}
	}
	, show: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			var child = this.childNodes[i];
			child.visible = true;
		}
	}
	, hide: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			var child = this.childNodes[i];
			child.visible = false;
		}
	}

});
