/**
 * spriteEffect.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SpriteEffect = Class.create(Sprite,{
	initialize: function(width, height) {
		if (typeof width === 'undefined')	width = 190;
		if (typeof height === 'undefined')	height = 190;
		// Parent Call
		Sprite.call(this, width, height);

		// Common Settings
		this.surface			= new AdvancedSurface(width, height);

		// Surface Settings
		this.image = this.surface;
	}

	/**
	 * in Battle Scene, attackEffect
	 */
	, attackEffect: function(battler) {
		if ("str" == battler.getType()) {
			this.fire();
		} else if ("def" == battler.getType()) {
			this.blizzard();
		} else if ("mgc" == battler.getType()) {
			this.thunder();
		}
	}

	/**
	 * Attack Effects
	 */
	, fire: function() {
		this.setSpriteSheet("fire");
		this.setFrame("fire");
		this.setDisposeEvent();
	}
	, blizzard: function() {
		this.setSpriteSheet("blizzard");
		this.setFrame("blizzard");
		this.setDisposeEvent();
	}
	, thunder: function() {
		this.setSpriteSheet("thunder");
		this.setFrame("thunder");
		this.setDisposeEvent();
	}
	, setSpriteSheet: function(type) {
		if (type == "fire") {
			this.image = Game.Seq.core.assets['images/loa/fire.png'];
		} else if (type == "blizzard") {
			this.image = Game.Seq.core.assets['images/loa/blizzard.png'];
		} else if (type == "thunder") {
			this.image = Game.Seq.core.assets['images/loa/thunder.png'];
		}
	}
	, setFrame: function(type) {
		if (type == "fire") {
			this.frame = [0,1,2,3,4,5,6];
		} else if (type == "blizzard") {
			this.frame = [0,1,2,3,4,5,6];
		} else if (type == "thunder") {
			this.frame = [0,1,2,3,4,5,6];
		}
	}
	, setDisposeEvent: function() {
		this.tl.cue({
			7: function(){ 
				// remove SpriteEffect
				//this.tl.removeFromScene();
				this.parentNode.removeChild(this);
			},
		});
	}

});

