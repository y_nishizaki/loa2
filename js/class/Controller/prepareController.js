/**
 * prepareController.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var PrepareController = new function() {

	var self = function PrepareController() {
		Controller.apply(this, arguments);
	}

	var uber = Controller.prototype;

	self.prototype = extend(uber, {
		constructor: self
		/**
		 * Prepare Reset 
		 */
		, reset: function() {
			this.model.reset();
			this.unsetAllSprites();
			this.draw();
		}

		/**
		 * 戦闘開始をタッチ
		 */
		, touchStartBattle: function() {
			if (this.model.isPrepareComplete()) {
				// battle.friends.formation を更新
				var formation = this.model.getFormation();
				Game.Seq.battle.setFriendsFormation(formation);
				// Battle Sceneへ
				Game.Seq.changeScene(Game.Seq.battleScene);
				// Battle 開始
				Game.Seq.battleController.start();
			}
		}

		/**
		 * ストックをタッチ
		 */
		, touchStock: function(e) {
			// タッチした位置を判別してFormationに新しいUnitを追加する
			var job_type = this.scene.stockTable.selectJob(e.x, e.y);
			if (job_type) {
				// Formationに空白マスがあるかどうか判定
				if (this.model.formation.searchEmpty()) {
					// StockからタッチしたJobのユニットを1コ減らす
					if (this.model.stock.removeUnit(job_type)) {
						// FormationにタッチしたJobのユニットを1コ増やす
						var unit = this.model.formation.addUnit(job_type);
						if (unit) {
							// 新しいSpriteUnitを追加する
							this.setSpriteUnit(unit);
						}
					}
				}
			}
			// Update Prepare Scene
			Game.Seq.prepareController.draw();
			// Debug Console
			this.model.outputConsole();
		}

		/**
		 * 新しいSpriteUnitを生成しFormationに追加する
		 */
		, setSpriteUnit: function(unit) {
			// SpriteUnit
			var sprite  = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
			this.scene.formationTable.addChild(sprite);

			// Scene target unit
			this.scene.sprite	= sprite;
			this.scene.sprites.push(sprite);

			// Position Settings
			this.scene.sprite.setUnit(unit);
			this.scene.sprite.setPos();
			// Event Settings
			this.scene.sprite.dragOn();
			this.scene.sprite.addEventListener(enchant.Event.TOUCH_START, this.touchSpriteUnit);
			this.scene.sprite.addEventListener(enchant.Event.TOUCH_END, this.dragEnd);

			// New Sprite is Blink
			this.selectOffAllSprites();
			this.scene.sprite.selectOn();

			// Set Selected Block
			this.scene.formationTable.selectedBlock.setPos(unit.rowIndex, unit.lineIndex);
		}

		/**
		 * ユニットのスプライトをタッチ
		 */
		, touchSpriteUnit: function(e) {
			// Select Off
			Game.Seq.prepareController.selectOffAllSprites();
			// Select On
			this.scene.sprite = this;
			this.scene.sprite.selectOn();
			// Debug Console
			if (Game.Debug) {
				console.log("unit", this.unit);
			}
		}
		, selectOffAllSprites: function() {
			for (var i=0; i<this.scene.sprites.length; i++) {
				this.scene.sprites[i].selectOff();
			}
		}

		/**
		 * ユニットをドラッグして放す
		 */
		, dragEnd: function(e) {
			// ドラッグを終了した位置の取得
			var rowIndex  = this.scene.formationTable.getRowIndex(e.x);
			var lineIndex = this.scene.formationTable.getLineIndex(e.y);

			// 陣形の外でドラッグを終了した場合の処理
			if (rowIndex === false || lineIndex === false) {
				this.setPos();
				// ストックの上でドラッグを終了した場合、手持ちにユニットを戻す
				if (260 < e.y) {
					Game.Seq.prepareController.returnUnit();
				}
				return false;
			}

			// ドラッグ前後で位置が変わらなかった場合、元の位置に戻す
			if (this.unit.rowIndex == rowIndex && this.unit.lineIndex == lineIndex) {
				this.setPos();
				return false;
			}

			// ドラッグ先の場所にユニットを移動させる
			var unit	  = this.scene.sprite.unit;
			var start_pos = new Array(unit.rowIndex, unit.lineIndex);
			var end_pos   = new Array(rowIndex, lineIndex);
			this.scene.model.formation.swapPos(start_pos, end_pos);

			// Update 
			this.scene.model.formation.update();
			this.scene.formationTable.setUnitsPos();
			Game.Seq.prepareController.draw();

			if (Debug) {
				console.log("start_pos", start_pos);
				console.log("end_pos", end_pos);
				console.log("Formation.units", this.scene.model.formation.units);
				this.scene.model.formation.arrangement.viewList();
			}

		}

		/**
		 * 手持ちにユニットを戻す処理
		 */
		, returnUnit: function() {
			// 戦闘状態の判別
			var battleBefore = Game.Seq.battle.isBeforeBattle();
			// 戦闘状態が戦闘前の場合のみFormationからStockに移す
			if (battleBefore) {
				var sprite	  = this.scene.sprite;
				var unit	  = this.scene.sprite.unit;
				var units	  = this.model.formation.getUnits();
				var job_type  = unit.getJobType();

				// ドラッグしているユニットを判別してFormationから取り除く
				var index;                                                                   
				for (var i=0; i<units.length; i++) {    
					if (units[i] === unit) {            
						index = i;                      
					}                                   
				}                                       
				if (typeof index !== 'undefined') {     
					units.splice(index, 1);                                      
				}                                                                            
				// Update
				this.model.formation.update();
				this.model.stock.addUnit(job_type);
				this.scene.formationTable.removeChild(sprite);
				this.draw();
			}
		}

		/**
		 * タクティクスフェーズで呼び出される
		 *   Formationのスプライトを新しく作成し配置する
		 */
		, setFormationSprites: function() {
			// Friend formation
			var units = this.model.formation.getUnits();
			for (var i=0; i<units.length; i++) {
				var unit = units[i];
				// Sprite
				this.setSpriteUnit(unit);
			}
		}

		/**
		 * Draw Scene
		 */
		, draw: function() {
			this.drawDisplay();
			this.drawStock();
			this.drawSynchroLines();
		}
		, drawDisplay: function() {
			this.scene.displayTable.update();
		}
		, drawStock: function() {
			// ユニット個数の更新
			this.scene.stockTable.update();
			// 戦闘準備中のみ表示する
			if (Game.Seq.battle.isBeforeBattle()) {
				this.scene.stockTable.show();
				this.scene.enemyTable.hide();
			} else {
				this.scene.stockTable.hide();
				this.scene.enemyTable.show();
				// 前回のスプライトを破棄
				this.scene.enemyTable.clear();
				// 戦闘相手の情報を描画する
				var enemy = Game.Seq.battle.enemy;
				var units = enemy.getUnits();
				for (var i=0; i<units.length; i++) {
					var unit = units[i];
					var sprite  = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
					this.scene.enemyTable.units.addChild(sprite);
					sprite.setUnit(unit);
					sprite.setPos();
				}
				// 戦闘相手のステータスを描画する
				this.scene.enemyTable.update();

			}
		}
		, drawSynchroLines: function() {
			this.scene.formationTable.synchroLines.update();
		}
	});

	return self;
}


