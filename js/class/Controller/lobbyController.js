/**
 * lobbyController.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var LobbyController = new function() {

	var self = function LobbyController() {
		Controller.apply(this, arguments);
	}

	var uber = Controller.prototype;

	self.prototype = extend(uber, {
		constructor: self
		/**
		 * Lobby Start 
		 */
		, start: function() {
			// Challenger$B0lMw$rIA2h$9$k(B
			this.scene.challengerTable.initLabels();
		}
		, setTutorial: function() {
			// $B=i2s$N(BBattle$B$OD)@o<T$r8GDj$9$k(B
			var challenger_id = this.model.selectFirstId();
			if (typeof challenger_id === 'number') {
				var challenger = this.scene.model.getChallengerById(challenger_id);
				// Set Current Challenger
				this.model.setCurrentChallenger(challenger);
				// Set Enemy 
				this.setEnemy(challenger);
				// Set Tutorial
				this.touchChallenger(challenger_id);
				this.scene.introductionTable.btn_back.visible = false;
				this.scene.introductionTable.btn_next.visible = false;
				// $BM7$SJ}$rI=<($9$k(B
				var manual = this.model.isShowManual();
				if (manual) {
					this.clickManualBtn();
				}
			}
		}

		/**
		 * Touch Challenger Label
		 */
		, touchChallenger: function(id) {
			// Get Challenger Id
			var challenger_id = id;
			if (typeof challenger_id === 'number') {
				// Get Challenger
				var challenger = this.scene.model.getChallengerById(challenger_id);

				// challenger$B$,(BKnockDown$B$7$F$$$l$P@o$($J$$(B
				if (challenger.isKnockDown()) {
					return false;
				}

				// $B?@>R2p$K(Bchallenger$B$N>pJs$r:\$;$k(B
				this.scene.introductionTable.set(challenger);
				this.scene.introductionTable.show();
				// $BD)@o<T$N0lMw$OHsI=<($K$9$k(B
				this.scene.challengerTable.hide();
				// $BM7$SJ}$rHsI=<($K$7$F!"La$k$H?J$`$rI=<((B
			}
		}
		, touchIntroduction: function(id) {
			// Get Challenger Id
			var challenger_id = id;
			if (typeof challenger_id === 'number') {
				// Get Challenger
				var challenger = this.scene.model.getChallengerById(challenger_id);

				// challenger$B$,(BKnockDown$B$7$F$$$l$P@o$($J$$(B
				if (challenger.isKnockDown()) {
					return false;
				}

				// $B?@>R2p$r%j%;%C%H$7$FHsI=<($K$9$k(B
				this.scene.introductionTable.unset();
				this.scene.introductionTable.hide();
				// $BD)@o<T$N0lMw$rI=<($9$k(B
				this.scene.challengerTable.show();
				
				// $B@oF.3+;O(B!
				this.scene.model.setCurrentChallenger(challenger);
				this.setEnemy(challenger);
				Game.Seq.battleController.reset();
				Game.Seq.prepareController.reset();
				// Change Scene
				Game.Seq.changeScene(Game.Seq.prepareScene);
			}
		}
		, touchBack: function() {
			// $B?@>R2p$r%j%;%C%H$7$FHsI=<($K$9$k(B
			this.scene.introductionTable.unset();
			this.scene.introductionTable.hide();
			// $BD)@o<T$N0lMw$rI=<($9$k(B
			this.scene.challengerTable.show();
		}

		/**
		 * Set Battle Enemy
		 */
		, setEnemy: function(challenger) {
			// Set Enemy Name
			var enemy_name = challenger.getName();
			Game.Seq.battle.enemy.setBattlerName(enemy_name);
			// Set Enemy Formation
			var list = challenger.getList();
			var formation = new Formation();
			formation.setList(list);
			Game.Seq.battle.enemy.setFormation(formation);
			// Set Enemy AI
			var ai = challenger.getAI();
			Game.Seq.battle.enemy.setAI(ai);
			// Set Enemy Petterns
			var patterns = challenger.getPatterns();
			Game.Seq.battle.enemy.setPatterns(patterns);
		}

		/**
		 * knockDown
		 */
		, knockDownCurrentChallenger: function() {
			// current_challenger$B$r(BKnockDown
			this.model.current_challenger.setKnockDown(true);
			// challenger$B$NL>A0$KBG$A>C$7@~$rDI2C$9$k(B
			var id = this.model.current_challenger.getId();
			this.scene.challengerTable.setKnockDown(id);
			// current_challenger$B$r:o=|(B
			this.model.current_challenger = null;
		}

		/**
		 * Playing Manual
		 */
		, clickManualBtn: function() {
			// Playing Manual
			this.scene.playinManualImage01 = new Sprite(GAME_WIDTH, GAME_HEIGHT);
			this.scene.playinManualImage01.image = Game.Seq.core.assets['images/loa/manual01.png'];
			this.scene.playinManualImage01.x = 0;
			this.scene.playinManualImage01.y = 0;
			this.scene.addChild(this.scene.playinManualImage01);
			this.addNext();
			// Event Settings
			this.scene.playinManualImage01.addEventListener(enchant.Event.TOUCH_START, function(e){
				Game.Seq.lobbyController.removeNext();
				this.scene.removeChild(this);
				Game.Seq.lobbyController.clickManual01();
			});
		}
		, clickManual01: function() {
			// Playing Manual2
			this.scene.playinManualImage02 = new Sprite(GAME_WIDTH, GAME_HEIGHT);
			this.scene.playinManualImage02.image = Game.Seq.core.assets['images/loa/manual02.png'];
			this.scene.playinManualImage02.x = 0;
			this.scene.playinManualImage02.y = 0;
			this.scene.addChild(this.scene.playinManualImage02);
			this.addNext();
			// Event Settings
			this.scene.playinManualImage02.addEventListener(enchant.Event.TOUCH_START, function(e){
				Game.Seq.lobbyController.removeNext();
				this.scene.removeChild(this);
				Game.Seq.lobbyController.clickManual02();
			});
		}
		, clickManual02: function() {
			// Playing Manual3
			this.scene.playinManualImage03 = new Sprite(GAME_WIDTH, GAME_HEIGHT);
			this.scene.playinManualImage03.image = Game.Seq.core.assets['images/loa/manual03.png'];
			this.scene.playinManualImage03.x = 0;
			this.scene.playinManualImage03.y = 0;
			this.scene.addChild(this.scene.playinManualImage03);
			this.addNext();
			// Event Settings
			this.scene.playinManualImage03.addEventListener(enchant.Event.TOUCH_START, function(e){
				Game.Seq.lobbyController.removeNext();
				this.scene.removeChild(this);
				Game.Seq.lobbyController.clickManual03();
			});
		}
		, clickManual03: function() {
			// Playing Manual3
			this.scene.playinManualImage04 = new Sprite(GAME_WIDTH, GAME_HEIGHT);
			this.scene.playinManualImage04.image = Game.Seq.core.assets['images/loa/manual04.png'];
			this.scene.playinManualImage04.x = 0;
			this.scene.playinManualImage04.y = 0;
			this.scene.addChild(this.scene.playinManualImage04);
			this.addNext();
			// Event Settings
			this.scene.playinManualImage04.addEventListener(enchant.Event.TOUCH_START, function(e){
				Game.Seq.lobbyController.removeNext();
				this.scene.removeChild(this);
			});
		}
		, addNext: function() {
			// Next
			this.scene.next = new SpriteBase(L_BTN_WIDTH, L_BTN_HEIGHT);
			this.scene.next.image = Game.Seq.core.assets['images/loa/btn_next.png'];
			this.scene.next.x = 240;
			this.scene.next.y = 280;
			this.scene.next.touchEnabled = false;
			this.scene.addChild(this.scene.next);
			// $BE@LG$5$;$k(B
			this.scene.next.flushOn();
		}
		, removeNext: function() {
			this.scene.removeChild(this.scene.next);
		}

		/**
		 * Game Clear
		 */
		, judgeGameClear: function() {
			if (this.model.isGameClear()) {
				// Congratulations!
				this.scene.appearCongratulations();
				// $BM7$SJ}$HBP@oAj<j$r%?%C%A$7$F$/$@$5$$$rHsI=<((B
				this.scene.challengerTable.btn_manual.visible = false;
				this.scene.challengerTable.explain.visible = false;
				// Ending$B$rI=<($9$k%\%?%s$rI=<((B
				this.scene.btn_next.visible = true;
				this.scene.btn_next.flushOn();
			}
		}
		, showGameClear: function() {
			if (this.model.isGameClear()) {
				// Twitter$BEj9F2hLL$X(B
				var score = Game.Seq.battle.getCount();
				Game.Seq.core.end(score, '#LegendOfAsgard ' + score + '$B2s$N@oF.$G%/%j%"$7$^$7$?(B');
				//  // Game Clear
				//  this.scene.gameClearImage = new Sprite(GAME_WIDTH, GAME_HEIGHT);
				//  this.scene.gameClearImage.image = Game.Seq.core.assets['images/loa/ending.png'];
				//  this.scene.gameClearImage.x = 0;
				//  this.scene.gameClearImage.y = 0;
				//  this.scene.addChild(this.scene.gameClearImage);
				//  // Event Settings
				//  this.scene.gameClearImage.addEventListener(enchant.Event.TOUCH_START, function(e){
				//  	Game.Seq.lobbyController.transferTwitterScore();
				//  });
			}
		}
		/*
		, transferTwitterScore: function() {
			// Twitter$BEj9F2hLL$X(B
			var score = Game.Seq.battle.getCount();
			Game.Seq.core.end(score, '#LegendOfAsgard ' + score + '$B2s$N@oF.$G%/%j%"$7$^$7$?(B');
		}
		*/

	});

	return self;
}


