/**
 * debugController.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var DebugController = new function() {

	var self = function DebugController() {
		Controller.apply(this, arguments);
		console.log("DebugControllerのコンストラクタが呼ばれました");
		console.log(this.model);
		console.log(this.scene);
	}

	var uber = Controller.prototype;

	self.prototype = extend(uber, {
		constructor: self
		/**
		 * Sprite Base
		 */
		, spriteBaseTest: function() {
			// 新しくスプライトを出現させる
			var unit	= new Unit("Knight");
			var sprite	= new SpriteBase(UNIT_WIDTH, UNIT_HEIGHT);
			sprite.setUnit(unit);
			sprite.x = 10;
			sprite.y = 10;
			console.log("spriteBaseのテストを開始！");
			this.scene.addChild(sprite);
			
			// target
			this.scene.unit		= unit;
			this.scene.sprite	= sprite;
			this.scene.sprites.push(sprite);

			// Event Settings
			this.scene.sprite.addEventListener(enchant.Event.TOUCH_START, this.selectSprite);
			this.scene.sprite.addEventListener(enchant.Event.TOUCH_START, this.selectOffAllSprites);
			this.selectOffAllSprites();
			this.scene.sprite.selectOn();
			this.job_count		= 1;

			// Debug Console
			console.log(sprite);
			console.log(this.scene);
		}

		/**
		 * SpriteBase.select
		 */
		, selectSprite: function(e) {
			console.log("selectSprite");
			this.scene.sprite = this;
			this.scene.sprite.selectOn();
		}
		, selectOffAllSprites: function(e) {
			console.log("selectOffAllSprites");
			for (var i=0; i<this.scene.sprites.length; i++) {
				this.scene.sprites[i].selectOff();
			}
		}

		/**
		 * SpriteBase.mirror
		 */
		, turnMirror: function() {
			if (this.scene.sprite.isMirror()) {
				console.log("スプライトの反転を戻します");
				this.scene.sprite.mirrorOff();
			} else {
				console.log("スプライトを反転させます");
				this.scene.sprite.mirrorOn();
			}
		}

		/**
		 * SpriteBase.blink
		 */
		, turnBlink: function() {
			if (this.scene.sprite.isBlink()) {
				console.log("スプライトのブリンクを解除します");
				this.scene.sprite.blinkOff();
			} else {
				console.log("スプライトをブリンクさせます");
				this.scene.sprite.blinkOn();
			}
		}

		/**
		 * SpriteBase.setUnit()
		 */
		, turnJob: function() {
			console.log("スプライトのJobを変更します");
			if (typeof this.job_count === 'undefined')	this.job_count = 0;
			var job_name = "";
			switch (this.job_count) {
				case 0:
					job_name = "Knight";
					break;
				case 1:
					job_name = "Guardian";
					break;
				case 2:
					job_name = "Wizard";
					break;
				case 3:
					job_name = "Bridge";
					break;
				case 4:
					job_name = "Terminator";
					break;
				case 5:
					job_name = "Berserker";
					break;
				default:
					job_name = "Knight";
					this.job_count = null;
					break;
			}
			this.job_count++;
			this.scene.unit.changeJob(job_name);
			this.scene.sprite.setUnit(this.scene.unit);
			console.log(job_name + "に変更しました");
		}

		/**
		 * SpriteBase.collapse()
		 */
		, effectCollapse: function() {
			console.log("スプライトに死亡エフェクトを与えます");
			this.scene.sprite.collapse();
		}

		/**
		 * SpriteBase.damage()
		 */
		, effectDamage: function() {
			var damage	= Math.floor(Math.random()*500);
			var crt		= Math.floor(Math.random()*100);
			if (crt>50) crt = true;
			else		crt = false;
			this.scene.sprite.damage(damage, crt);

			console.log("スプライトに" + damage +"のダメージを与えます");
			if (crt) { 
				console.log("クリティカル！");
			}
		}

		/**
		 * SpriteBase.drag
		 */
		, turnDrag: function() {
			if (this.scene.sprite.isDragable()) {
				console.log("スプライトのドラッグを解除します");
				this.scene.sprite.dragOff();
			} else {
				console.log("スプライトをドラッグ可能にします");
				this.scene.sprite.dragOn();
			}
		}

		/**
		 * SpriteBase.flush
		 */
		, turnFlush: function() {
			if (this.scene.sprite.isFlush()) {
				console.log("スプライトの点滅を解除します");
				this.scene.sprite.flushOff();
			} else {
				console.log("スプライトを点滅させます");
				this.scene.sprite.flushOn();
			}
		}

		/**
		 * Sprite Effect
		 */
		, damageTest: function(e) {
			console.log("ダメージエフェクトのテスト");
			var value		= Math.floor(Math.random()*500);
			var damage_type	= Math.floor(Math.random()*3);
			// SpriteDamage
			var sprite	= new SpriteDamage(value, damage_type);
			sprite.x = 0;
			sprite.y = e.y;
			// Damage 
			this.scene.addChild(sprite);
		}

		/**
		 * Fire Effect
		 */
		, fireTest: function(e) {
			var sprite	= new SpriteEffect();
			sprite.x = 0;
			sprite.y = 0;
			this.scene.addChild(sprite);
			sprite.fire();
			console.log("spriteEffect", sprite);
			console.log("sprite._frameSequence", sprite._frameSequence);
			console.log("sprite._frameSequence.length", sprite._frameSequence.length);
		}
		/**
		 * Blizzard Effect
		 */
		, blizzardTest: function(e) {
			var sprite	= new SpriteEffect();
			sprite.x = 0;
			sprite.y = 0;
			this.scene.addChild(sprite);
			sprite.blizzard();
		}
		/**
		 * Thunder Effect
		 */
		, thunderTest: function(e) {
			var sprite	= new SpriteEffect();
			sprite.x = 0;
			sprite.y = 0;
			this.scene.addChild(sprite);
			sprite.thunder();
		}
	});

	return self;
}


