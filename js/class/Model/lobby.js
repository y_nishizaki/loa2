/**
 * lobby.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Lobby = new function() {

	var self = function Lobby() {
		SceneState.apply(this, arguments);

		// Common Settings
		this.challengers	= new Array();
		this.challenger_ids = new Array();
		this.game_clear			= false;
		this.current_challenger = null;
		this.show_manual = false;

		// Challenger init
		//var ids = new Array(1,2);
		var ids = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
		// Set Challenger Ids
		this.setChallengerIds(ids);
		// Make Challenger list
		this.makeChallengers();
	}

	var uber = SceneState.prototype;

	self.prototype = extend(uber, {
		constructor: self
		/**
		 * getter
		 */
		, getChallengers: function() {
			return this.challengers;
		}
		, getCurrentChallenger: function() {
			return this.current_challenger;
		}
		, getChallengerIds: function() {
			return this.challenger_ids;
		}

		/**
		 * Get Random Challenger
		 */
		, getRandomChallenger: function() {
			// $BE]$7$F$$$J$$(Bchallenger$B$r0lBNA*$V(B
			var id = this.selectRandomId();
			var challenger = this.getChallengerById(id);
			if (challenger) {
				// $BA*Br$5$l$?(Bchallenger$B$rJ];}$7$F$*$/(B
				this.setCurrentChallenger(challenger);
				return challenger;
			}
			return false;
		}
		, getChallengerById: function(id) {
			for (var i=0; i<this.challengers.length; i++) {
				var challenger = this.challengers[i];
				// $B0z?t$N(Bid$B$H0lCW$9$k(Bchallenger$B$rH=Dj$9$k(B
				if (challenger.getId() == id) {
					return challenger;
				}
			}
			return false;
		}

		/**
		 * boolean
		 */
		, isGameClear: function() {
			var cnt = 0;
			for (var i=0; i<this.challengers.length; i++) {
				var challenger = this.challengers[i];
				// challenger$B$rE]$7$F$$$l$P%+%&%s%H%"%C%W(B
				if (challenger.isKnockDown()) {
					cnt++;
				}
			}
			// challenger$B$rA40wE]$;$P%2!<%`%/%j%"(B
			if (cnt == this.challengers.length) {
				this.game_clear = true;
			}
			return this.game_clear;
		}
		, isShowManual: function() {
			return this.show_manual;
		}

		/**
		 * setter
		 */
		, setChallengerIds: function(ids) {
			this.challenger_ids = ids;
		}
		, setCurrentChallenger: function(challenger) {
			this.current_challenger = challenger;
		}
		, setShowManual: function(manual) {
			this.show_manual = manual;
		}

		/**
		 * in initialize, make challenger list
		 */
		, makeChallengers: function() {
			//console.log("make challenger");
			//console.log("challenger ids", this.challengger_ids);
			this.challengers = new Array();
			for (var i=0; i<this.challenger_ids.length; i++) {
				var id = this.challenger_ids[i];
				var challenger = new Challenger(id);
				//console.log("new challenger", challenger);
				// add challenger
				this.challengers.push(challenger);
			}
		}

		/**
		 * to Judge GameClear, count Reserve Challengers
		 */
		, countReserveChallenger: function() {
			var cnt = 0;
			for (var i=0; i<this.challengers.length; i++) {
				var challenger = this.challengers[i];
				// challenger$B$rE]$7$F$$$J$1$l$P%+%&%s%H%"%C%W(B
				if (!challenger.isKnockDown()) {
					cnt++;
				}
			}
			return cnt;
		}

		/**
		 * in Reserve Challengers, select a challenger
		 */
		, selectRandomId: function() {
			var reserve_ids = new Array();
			for (var i=0; i<this.challengers.length; i++) {
				var challenger = this.challengers[i];
				// challenger$B$rE]$7$F$$$J$1$l$P(Breserve_ids$B$KDI2C(B
				if (!challenger.isKnockDown()) {
					var id = challenger.getId();
					reserve_ids.push(id);
				}
			}
			var l = reserve_ids.length;
			var rand = Math.floor(Math.random() * l);
			var select_id = reserve_ids[rand];
			if (select_id) {
				return select_id;
			}
			return false;
		}

		/**
		 * first battle challenger id
		 */
		, selectFirstId: function() {
			// $B%A%c%l%s%8%c!<(Bid$B$+$i#0HVL\$rA*$V(B
			var ids = this.getChallengerIds();
			if (ids.length > 0) {
				return ids[0];
			}
			return false;
		}


	});

	return self;
}

/*
- challengers
- game_clear
- current_challenger
- challenger_ids
- getChallengers()
- getChallenger(Challenger.id)
- getCurrentChallenger()
- isGameClear()
- setChallengerIds(array)
- setCurrentChallenger()
- makeChallengers()
- countReserveChallenger()
- selectRandomId()
*/
