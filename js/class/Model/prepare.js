/**
 * prepare.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Prepare = new function() {
	var stock;
	var formation;
	var battler_hp;

	var self = function Prepare(stock_args) {
		SceneState.apply(this, arguments);

		if (typeof stock_args === 'undefined') {
			stock_args = {
				"Knight"	: 9,
				"Guardian"	: 9,
				"Wizard"	: 9,
				"Bridge"	: 3,
				"Terminator": 3,
				"Berserker"	: 6
			}
			if (Game.StockDebug) {
				stock_args = {
					"Knight"	: 99,
					"Guardian"	: 99,
					"Wizard"	: 99,
					"Bridge"	: 99,
					"Terminator": 99,
					"Berserker"	: 99
				}
			}
		}
		this.stock		= new Stock(stock_args);
		this.formation	= new Formation();
		this.resetBattlerHp();
	}

	var uber = SceneState.prototype;

	self.prototype = extend(uber, {
		constructor: self
		/**
		 * getter
		 */
		, getFormation: function(formation) {
			return this.formation;
		}
		, getBattlerHp: function() {
			if (typeof this.battler_hp === 'undefined') return false;
			return this.battler_hp;
		}

		/**
		 * boolean
		 */
		, isPrepareComplete: function() {
			// Formationに少なくとも一体ユニットを配置していれば戦闘開始が可能
			if (1 <= this.formation.units.length) {
				return true;
			}
			return false;
		}

		/**
		 * setter
		 */
		, setFormation: function(formation) {
			this.formation = formation;
		}

		/**
		 * reset
		 */
		, reset: function() {
			// Formationは毎回の戦闘で初期化する
			this.formation	= new Formation();
			this.setReserveStock();
			this.resetBattlerHp();
		}

		/**
		 * in Lobby, reset stock
		 */
		, setReserveStock: function() {
			var defaults = {
				"K"		: 9,
				"G"		: 9,
				"W"		: 9,
				"Br"	: 3,
				"Tr"	: 3,
				"Bs"	: 6
			}
			// 現在のFormationからStockを計算する
			  var stock_args = new Array();
			  stock_args["K"] = defaults["K"] - this.formation.countUnit("K");
			  stock_args["G"] = defaults["G"] - this.formation.countUnit("G");
			  stock_args["W"] = defaults["W"] - this.formation.countUnit("W");
			  stock_args["Br"] = defaults["Br"] - this.formation.countUnit("Br");
			  stock_args["Tr"] = defaults["Tr"] - this.formation.countUnit("Tr");
			  stock_args["Bs"] = defaults["Bs"] - this.formation.countUnit("Bs");
			  this.stock = new Stock(stock_args);
		}

		/**
		 * in Battle TacticsPhase, Only Change formation
		 */
		, tacticsInit: function(battler) {
			// stockを空にする
			this.stock.vacate();
			// battler
			this.setFormation(battler.formation);
			this.battler_hp = battler.getHp();
		}

		/**
		 * not In Battle, battler_hp is undefined
		 */
		, resetBattlerHp: function() {
			this.battler_hp	= undefined;
		}

		/**
		 * Debug Console
		 */
		, outputConsole: function() {
			if (Game.Debug) {
				console.log("stockの状態：");
				this.stock.debugState();
				console.log("formationの状態：");
				this.formation.debugState();
			}
		}
	});

	return self;
}
