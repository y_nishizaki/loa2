/**
 * myprototype.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Class Extend 
 */
function extend(o) {
  var f = extend.f, i, len, n, prop;
  f.prototype = o;
  n = new f;
  for (i=1, len=arguments.length; i<len; ++i) {
    for (prop in arguments[i]) {
      n[prop] = arguments[i][prop];
    }
  }
  return n;
}
extend.f = function(){};

/**
 * Array Extend 
 */
Array.prototype.clone = function(){
	return Array.apply(null,this)
}


/**
 * FPS
 * @author	  http://tech.kayac.com/archive/canvas-tutorial.html
 */

var FPS = function(target) {
	this.target     = target;        // 目標FPS
	this.interval   = 1000 / target; // setTimeoutに与えるインターバル
	this.checkpoint = new Date();
	this.fps        = 0;
};
FPS.prototype = {
	// checkからcheckまでの時間を元にFPSを計算
	check: function() {
		var now = new Date();
		this.fps = 1000 / (now - this.checkpoint);
		this.checkpoint = new Date();
	},
	// 現在のFPSを取得
	getFPS: function() {
		return this.fps.toFixed(2);
	},
	// 次回処理までのインターバルを取得
	getInterval: function() {
		var elapsed = new Date() - this.checkpoint;
		return this.interval - elapsed > 10 ? this.interval - elapsed : 10;
	}
};

