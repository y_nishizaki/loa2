/**
 * Chaineder MVC Framework
 *   Game has a Sequence Object.
 *   Sequence has 
 * 		an enchant.js core object,	
 *	and	Application Models,			(Model)
 * 	and enchant.js Scene Clases.	(View) 
 * 	and	Application Controllers.	(Controller)
 *
 *   Controller is assosiated EventTarget object.
 *   ex)	command1.addEventListener(enchant.Event.TOUCH_START, function(e) {
 * 			Game.Seq.StartingController.hoge();
 * 		}
 */

enchant();

var Game;				// Global Object
Game.Debug			= false;		// Debug Settings
Game.DAMAGE_TYPE	= 1;		// Damage Settings
Game.StockDebug		= false;		// Stock x 99

window.onload = function() {
	/**
	 *  ---------------------------------
	 *   enchant.js Application Settings
	 *  ---------------------------------
	 */
	var Seq = new Sequence();
	Seq.core.fps = 30;
	Seq.core.preload('images/icon1.png');
	Seq.core.preload('images/loa/element.png');
	Seq.core.preload('images/loa/bg01.png');
	Seq.core.preload('images/loa/bg02.png');
	Seq.core.preload('images/loa/bg03.png');
	Seq.core.preload('images/loa/battle_status.png');
	Seq.core.preload('images/loa/battle.png');
	Seq.core.preload('images/loa/prepare_status.png');
	Seq.core.preload('images/loa/prepare_stock.png');
	Seq.core.preload('images/loa/prepare_enemy.png');
	Seq.core.preload('images/loa/formation_frame.png');
	Seq.core.preload('images/loa/lobby.png');
	Seq.core.preload('images/loa/title.png');
	Seq.core.preload('images/loa/touch_start_mini.png');
	Seq.core.preload('images/loa/battle_start_01_mini.png');
	Seq.core.preload('images/loa/battle_start_02_mini.png');
	Seq.core.preload('images/loa/title.png');
	Seq.core.preload('images/loa/opening.png');
	Seq.core.preload('images/loa/ending.png');
	Seq.core.preload('images/loa/fire.png');
	Seq.core.preload('images/loa/blizzard.png');
	Seq.core.preload('images/loa/thunder.png');
	Seq.core.preload('images/loa/manual01.png');
	Seq.core.preload('images/loa/manual02.png');
	Seq.core.preload('images/loa/manual03.png');
	Seq.core.preload('images/loa/manual04.png');
	Seq.core.preload('images/loa/number01.png');
	Seq.core.preload('images/loa/number02.png');
	Seq.core.preload('images/loa/line01.png');
	Seq.core.preload('images/loa/line02.png');
	Seq.core.preload('images/loa/line03.png');
	Seq.core.preload('images/loa/hp_bar.png');
	Seq.core.preload('images/loa/hp_frame.png');
	Seq.core.preload('images/loa/crystal.png');
	Seq.core.preload('images/loa/btn_next.png');
	Seq.core.preload('images/loa/btn_manual.png');
	Seq.core.preload('images/loa/btn_back.png');
	Seq.core.preload('images/loa/vs.png');
	Seq.core.preload('images/loa/congratulations.png');

	/**
	 *  --------------------------------
	 *      Chaineder Game Settings
	 *  --------------------------------
	 */
	Seq.core.onload = function() {
		console.log("LOA2");
		// Stage
		var STAGE_W = 300;
		var STAGE_H = 240;
		var stage = new Group(STAGE_W, STAGE_H);
		stage.x = 10;
		stage.y = 120;
		this.rootScene.addChild(stage);
		// Background
		var bg = new Sprite(STAGE_W, STAGE_H);
		var surface	 = new Surface(STAGE_W, STAGE_H);
		surface.context.strokeStyle = "black";
		surface.context.lineWidth	= 2;
		surface.context.lineJoin	= "round";
		// Formation Blocks
		var tx, ty = 0;
		var BLOCK_W = 50;
		var BLOCK_H = 50;
		var FORMATION_COLS = 4;
		var FORMATION_ROWS = 6;
		for (var i=0; i<FORMATION_COLS; i++) {
			for (var j=0; j<FORMATION_ROWS; j++) {
				tx = j * BLOCK_W;
				ty = i * BLOCK_H;
				surface.context.strokeRect(tx, ty, BLOCK_W, BLOCK_H);
			}
		}
		bg.image  = surface;	
		stage.addChild(bg);
		// Unit
		var units = new Array();
		for (var i=0; i<6; i++) {
			var spriteUnit = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
			var unit = new Unit("Knight");
			spriteUnit.setUnit(unit);
			stage.addChild(spriteUnit);
			spriteUnit.x = 50*i;
			units.push(spriteUnit);
		}
		// Flick
		var flicker = new Entity();
		flicker.touched = false;
		flicker.touchX = 0;
		flicker.touchY = 0;
		flicker.dir = "";
		flicker.radius = 20;
		flicker.time = 10;
		flicker.easing = enchant.Easing.CUBIC_EASEOUT;
		flicker.maxhold = 20;
		flicker.hold = 0;
		flicker.last_touched = 0;
		flicker.spriteUnit = null;
		stage.addChild(flicker);

		stage.addEventListener(enchant.Event.TOUCH_START, function(e){
			flicker.x = flicker.touchX = e.x - this.x;
			flicker.y = flicker.touchY = e.y - this.y;
			// タッチ座標のユニットを選択状態にする
			var selected = false;
			for (var i=0; i<units.length; i++) {
				var spriteUnit = units[i];
				if (flicker.within(spriteUnit, 25)) {
					selected = spriteUnit;
					break;
				}
			}
			if (selected) {
				flicker.touched = true;
				flicker.spriteUnit = selected;
				flicker.hold = 0;
			}
			// console.log(flicker);
			// console.log(this);
			// console.log(e);
			// ダブルタップ
			var progress = flicker.age-flicker.last_touched;
			console.log(progress);
			if (flicker.age-flicker.last_touched < 7) {
				console.log("ダブルタップ");
				var ball = new Sprite(16, 16);
				ball.image = Game.Seq.core.assets["images/icon1.png"];
				ball.frame = 2;
				ball.x = flicker.spriteUnit.x + stage.x;
				ball.y = flicker.spriteUnit.y + stage.y;
				Game.Seq.core.rootScene.addChild(ball);
				ball.tl.moveTo(160, 0, 20, enchant.Easing.CUBIC_EASEOUT);
			}
			flicker.last_touched = flicker.age;

		});
		stage.addEventListener(enchant.Event.TOUCH_MOVE, function(e){
			if (flicker.touched) {
				// フリック動作中の座標の検出
				flicker.x = e.x - this.x;
				flicker.y = e.y - this.y;
				var flag = flicker.spriteUnit.within(flicker, flicker.radius);
				// console.log("within", flag);
				// console.log("spriteUnit", spriteUnit);
				// console.log("flicker", flicker);
				// 一定距離がユニットから離れた時の処理
				if (flag == false) {
					var diff_x = flicker.x - flicker.touchX;
					var diff_y = flicker.y - flicker.touchY;
					var abs_x = Math.abs(diff_x);
					var abs_y = Math.abs(diff_y);
					if (abs_x > abs_y) {
						if (diff_x > 0)	flicker.dir = "right";
						else 			flicker.dir = "left";
	 				} else {
						if (diff_y > 0)	flicker.dir = "down";
						else 			flicker.dir = "up";
	 				}
	 				switch(flicker.dir) {
	 					case "right" :
	 						flicker.spriteUnit.tl.moveBy(BLOCK_W, 0, flicker.time, flicker.easing);
	 						break;
	 					case "left" :
	 						flicker.spriteUnit.tl.moveBy(-BLOCK_W, 0, flicker.time, flicker.easing);
	 						break;
	 					case "up" :
	 						flicker.spriteUnit.tl.moveBy(0, -BLOCK_H, flicker.time, flicker.easing);
	 						break;
	 					case "down" :
	 						flicker.spriteUnit.tl.moveBy(0, BLOCK_H, flicker.time, flicker.easing);
	 						break;
	 				};
					flicker.touched = false;
				}
			}
		});
		stage.addEventListener(enchant.Event.TOUCH_END, function(e){
			if (flicker.touched) {
				flicker.touched = false;
			}
		});
		stage.addEventListener(enchant.Event.ENTER_FRAME, function(e){
			if (flicker.touched) {
				// 長押しの判定
				flicker.hold++;
				if (flicker.hold >= flicker.maxhold) {
					console.log("hold!");
					flicker.touched = false;
					var ball = new Sprite(16, 16);
					ball.image = Game.Seq.core.assets["images/icon1.png"];
					Game.Seq.core.rootScene.addChild(ball);
					ball.x = flicker.spriteUnit.x + stage.x;
					ball.y = flicker.spriteUnit.y + stage.y;
					ball.tl.moveTo(160, 0, 20, enchant.Easing.BACK_EASEIN);
					console.log(flicker);
				}
			}
		});

	}

	/**
	 *  enchant.js Game Start
	 */
	Seq.core.start();
	Game.Seq = Seq;
};



